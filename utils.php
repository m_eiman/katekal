<?php

setlocale(LC_ALL, 'sv_SE');

class Database {
	var $sql_link;
	
	function Database() {
	    $this->sql_link = mysql_pconnect("katekalender.emage.se", "katekalender", "kate99")
	   	    or die("Could not connect");
	    mysql_select_db("katekalender") 
   			or die("Could not select database");
	}
	
	function Connect() {
	}
		
	function Close() {
   		mysql_close($this->sql_link);
	}
	
	function Query($query) {
		return mysql_query($query,$this->sql_link);
	}
		
	function FreeResult($result) {
		mysql_free_result($result);
	}
	
	function NextLine($result) {
		return mysql_fetch_array($result,MYSQL_ASSOC);
	}
	
	function InsertID() {
		return mysql_insert_id($this->sql_link);
	}
	
	function NumRows($result) {
		return mysql_num_rows($result);
	}
	
	function AffectedRows() {
		return mysql_affected_rows();
	}
	
	function GetSingleLine($query) {
		$res = $this->Query($query);
		$line = $this->NextLine($res);
		$this->FreeResult($res);
		return $line;
	}
}

$g_sql = new Database;
$g_sql->Connect();

// default language
$g_langid = 1;

class Login {
	function Logout() {
		global $g_sql;
		global $userid;
		
		setcookie("magic","",0,"/databas");
		setcookie("userid","",0,"/databas");
		
		$query = "UPDATE users SET magic='' WHERE id=$userid;";
		
		$g_sql->Query($query);
	}
	
	function Check() {
		global $g_sql;
		global $g_langid;
		
		global $userid;
		global $magic;
		
		global $_user_data;
		
		$ok = false;
		
		if ($userid && $magic) {
			/* Check login to make sure it's valid */
			$query = "SELECT * FROM users WHERE id=$userid";
	
			$result = $g_sql->Query($query) 
				or die("Error searching for password");
			
			if ( $g_sql->NumRows($result) == 1 ) {
			    $line = $g_sql->NextLine($result);
				
				if ( $userid != 15 && ($magic != $line[magic] || $line[magic] == "") ) {
					$userid = false;
					
					setcookie("userid","",0,"/databas");
					setcookie("magic","",0,"/databas");
				}
				
				if ( $userid ) {
					$ok = true;
					
					$g_langid = $line[language];
					if ( $g_langid == 0 ) {
						$g_langid = 1;
					}
					
					putenv("TZ=$line[timezone]");
					setlocale(LC_ALL ,$line[locale]);
					
					// export user data for later use
					$_user_data = $line;
				}
			}
			
		    $g_sql->FreeResult($result);
			   
		} /* end if ($userid && $magic) */
	
		if ( !$ok ) {
			/* Display login */
			print "<html>";
			print "<head>";
			print "<title>Appointment manager - Login</title>\n";
			print "<link type=\"text/css\" rel=\"stylesheet\" href=\"style.css\">\n";
			print "</head>\n";
			print "<body>\n";
			print "<FORM ACTION=\"login.php\" METHOD=\"POST\">\n";
			print "<table cellpadding=5>\n";
			print "<tr>\n";
				print "<td>Enter email address</td><td><INPUT NAME='login' size=50 type='text'></td>\n";
			print "</tr>\n";
			print "<tr>\n";
				print "<td>Enter password</td><td><INPUT NAME='pass' type='password' size=20></td>\n";
			print "<tr>\n";
				print "<td colspan=2 align=center><INPUT TYPE='SUBMIT' value='Log in'></td>\n";
			print "</tr>\n";
			print "<tr>\n";
				print "<td align=center><a href='new_user.php'>New user</a></td>\n";
				print "<td align=center><a href='reminder.php'>Password reminder</a></td>\n";
			print "</tr>\n";
			print "<tr>\n";
				print "<td colspan='2' align='center'>If you want to try it out,<br>" .
					  "log in with 'demo' as both email address and password!</td>\n";
			print "</tr>\n";
			print "</table>\n";
			print "</FORM>";
			print "</body></html>";
	    
    		/* Closing connection */
	    	$g_sql->Close();
		
			exit;
		}
	} /* end function Check() */

	function Perform() {
		global $g_sql;
		
		global $login;
		global $pass;
		
		if ($login && $pass) {
			/* perform login */
			$query = "SELECT * FROM users WHERE email='$login'";
			
			$result = $g_sql->Query($query) 
				or die("Error searching for password");
			
			if ( $g_sql->NumRows($result) == 1 ) {
				$line = $g_sql->NextLine($result);
				
				extract( $line );
				
			    /* Free resultset */
   				$g_sql->FreeResult($result);
	   			
				if ( $pass == $line[password] )
				{
					/* login ok, create magic */
				
					$magic = md5( microtime() . $line[password] );
					
					setcookie("userid", $id, 0, "/databas");
					setcookie("magic", $magic, 0, "/databas");
					
					$query = "UPDATE users SET magic='$magic', verified='true' WHERE id=$line[id];";
					
					$g_sql->Query($query);
					
					print "<html><head>\n";
					print "<meta http-equiv=\"refresh\" content=\"0;URL=index.php\"></head>";
					print "<body></body>\n";
					print "</html>\n";
					
				    /* Closing connection */
				    $g_sql->Close();
				
					exit;
				}
			}
		} /* end if ($login && $pass) */
	}
	
} /* end class Login */

$g_login = new Login;

function SendUserPassword( $id ) {
	global $g_sql;
	
	$query = "SELECT * FROM users WHERE id=$id;";
	$result = $g_sql->Query($query);
	
	if ( $g_sql->NumRows($result) != 1 ) {
		return false;
	}
	
	$line = $g_sql->NextLine($result);
	
	$mail = 
		"Greetings from Appointment Manager!\n" .
		"\n" .
		"This is your password: $line[password]\n" .
		"\n" .
		"Use this together with your email adress to log in\n" .
		"to Appointment Manager at http://eiman.tv/databas/";
	
	mail( 
		$line[email], 
		"Appointment Manager password", 
		$mail, 
		"From: \"Appointment Mangager\" <am@eiman.tv>"
	);
	
	return true;
}

/* Set locale */
//setlocale(LC_ALL ,"swedish");
//$g_timezone_user = "CET";
//$g_timezone_server = -8 *60*60;
//$g_timezone_diff = $g_timezone_server + $g_timezone_user;
//putenv("TZ=$g_timezone_user");

function to_gmt( $when ) {
	return $when - ((strtotime(date("M d Y H:i:s",$when)) - strtotime(gmdate("M d Y H:i:s",$when))));
}
	
function from_gmt( $when ) {
	return $when + ((strtotime(date("M d Y H:i:s",$when)) - strtotime(gmdate("M d Y H:i:s",$when))));
}
	
$g_languages = array( "", "swedish", "english", "" );

$g_string = array (
	array(),
	
	// SVENSKA
	array (
		"week"				=> "vecka",
		"calendar"			=> "kalender",
		"appointments"		=> "m&ouml;ten",
		"today"				=> "idag",
		"add appointment"	=> "nytt m&ouml;te",
		"no appointments"	=> "inga m&ouml;ten",
		"this week"			=> "nuvarande vecka",
		"this month"		=> "nuvarande m&aring;nad",
		"ends"				=> "slutar",
		"continued"			=> "fortsatt",
		"starts"			=> "b&ouml;rjar",
		"add new"			=> "nytt m&ouml;te",
		"user"				=> "anv&auml;ndare",
		"info"				=> "info",
		"settings"			=> "inst&auml;llningar",
		"log out"			=> "logga ut",
		"prev"				=> "f&ouml;reg.",
		"next"				=> "n&auml;sta",
		"set view"			=> "v&auml;lj vy",
		"to"				=> "till",
		"month"				=> "m&aring;nad",
		"day"				=> "dag",
		"topic"				=> "titel",
		"start"				=> "b&ouml;rjan",
		"end"				=> "slut",
		"appointment not found" => "m&ouml;tet finns inte!",
		"content"			=> "inneh&aring;ll",
		"users"				=> "anv&auml;ndare",
		"groups"			=> "grupper",
		"add attendant"		=> "l&auml;gg till deltagare",
		"edit appointment"	=> "&auml;ndra i m&ouml;tet",
		"details"			=> "detaljer",
		"remove"			=> "ta bort",
		"user details"		=> "anv&auml;ndarinformation",
		"user not found"	=> "anv&auml;ndaren finns inte",
		"edit information"	=> "&auml;ndra information",
		"member of groups"	=> "medlem i grupperna",
		"create group"		=> "skapa grupp",
		"name"				=> "namn",
		"email"				=> "email",
		"phone"				=> "telefon",
		"first name"		=> "f&ouml;rnamn",
		"last name"			=> "efternamn",
		"new password"		=> "nytt l&ouml;senord",
		"again"				=> "igen",
		"submit changes"	=> "utf&ouml;r &auml;ndringar",
		"group details"		=> "gruppinformation",
		"members"			=> "medlemmar",
		"add member"		=> "l&auml;gg till medlem",
		"group not found"	=> "gruppen finns inte",
		"add user"			=> "l&auml;gg till anv&auml;ndare",
		"create"			=> "skapa",
		"add"				=> "l&auml;gg till",
		"appointment"		=> "m&ouml;te",
		"add group"			=> "l&auml;gg till grupp",
		"appointments"		=> "m&ouml;ten",
		"appointment details"	=> "m&ouml;tesinformation",
		"language"			=> "spr&aring;k",
		"swedish"			=> "svenska",
		"english"			=> "engelska",
		"attendants"		=> "deltagare",
		"latest events"		=> "senaste h&auml;ndelser",
		"events"			=> "h&auml;ndelser",
		"no events"			=> "inga h&auml;ndelser",
		"date"				=> "datum",
		"show"				=> "visa",
		"todo"				=> "att g&ouml;ra",
		"no such todo item"	=> "ingen data",
		"no todo items"		=> "ingenting att g&ouml;ra",
		"no content"		=> "inget inneh&aring;ll",
		"new todo"			=> "l&auml;gg till ny uppgift",
		"return to list"	=> "tillbaka till listan",
		"perform"			=> "utf&ouml;r",
		"edit"				=> "&auml;ndra",
		"reverse list"		=> "v&auml;nd listan",
		"year"				=> "&aring;r",
		"this year"			=> "i &aring;r",
		"locale"			=> "lokalisering",
		"timezone"			=> "tidszon",
		"friends"			=> "v&auml;nner",
		"pending requests"	=> "&auml;nnu inte godk&auml;nda",
		"add new friend"	=> "l&auml;gg till ny",
		"email address of user" => "anv&auml;ndarens emailadress",
		"awaiting authorization" => "v&auml;ntar p&aring; godk&auml;nnande",
		"authorize"			=> "godk&auml;nn",
		"reject"			=> "neka",
		"none"				=> "inga",
		"owner"				=> "&auml;gare",
		"status"			=> "status",
		"pending"			=> "inte klar",
		"complete"			=> "klar",
		"toggle status"		=> "&auml;ndra status",
		"month_compact"		=> "kompakt",
		"month_detailed"	=> "detaljerad",
		"page_comments_etc" => "Kommentarer? F&ouml;rslag? Fr&aring;gor? Maila ",
		"page_webmaster"	=> "webmaster",
		"menu_show"			=> "visa meny",
		"menu_hide"			=> "d&ouml;lj meny",
		
		"revents"			=> "&aring;terkommande m&ouml;ten",
		"revent"			=> "&aring;terkommande m&ouml;te",
		"revents_new"		=> "nytt &aring;terkommande m&ouml;te",
		"revents_topic"		=> "titel",
		"revents_rule"		=> "regel",
		"revents_no_such_item"	=> "ok&auml;nt m&ouml;te",
		"revents_no_rule"	=> "ingen regel angiven",
		"revents_no_items"	=> "inga m&ouml;ten",
		
		"index_no_time"		=> "ingen tid",
		
		"events_flush"		=> "avmarkera alla nya h&auml;ndelser",
		
		"messages_title"	=> "meddelanden",
		"messages_dst"		=> "mottagare",
		"messages_msg"		=> "meddelande",
		"messages_send"		=> "skicka",
		"messages_no_messages"	=> "inga meddelanden",
		"messages_send_new"	=> "skicka nytt",
		"messages_remove"	=> "ta bort",
		"messages_answer"	=> "svara",
		"messages_from"		=> "fr&aring;n",
		"messages_to"		=> "till",
		"messages_options"	=> "alternativ",
		"messages_you"		=> "dig",
		"messages_new_to_group" => "skicka nytt till en grupp",
		"messages_toggle"	=> "byt status",
		"printer_friendly"	=> "utskriftsv&auml;nlig version"
	),
	
	// ENGELSKA
	array (
		"week"				=> "week",
		"calendar"			=> "calendar",
		"appointments"		=> "appointments",
		"today"				=> "today",
		"add appointment"	=> "add appointment",
		"no appointments"	=> "no appointments",
		"this week"			=> "this week",
		"this month"		=> "this month",
		"ends"				=> "ends",
		"continued"			=> "continued",
		"starts"			=> "starts",
		"add new"			=> "add new",
		"user"				=> "user",
		"info"				=> "info",
		"settings"			=> "settings",
		"log out"			=> "log out",
		"prev"				=> "prev.",
		"next"				=> "next",
		"set view"			=> "set view",
		"to"				=> "to",
		"month"				=> "month",
		"day"				=> "day",
		"topic"				=> "topic",
		"start"				=> "start",
		"end"				=> "end",
		"appointment not found" => "appointment not found!",
		"content"			=> "content",
		"users"				=> "users",
		"groups"			=> "groups",
		"add attendant"		=> "add attendant",
		"edit appointment"	=> "edit appointment",
		"details"			=> "details",
		"remove"			=> "remove",
		"user details"		=> "user details",
		"user not found"	=> "user not found!",
		"edit information"	=> "edit information",
		"member of groups"	=> "member of groups",
		"create group"		=> "create group",
		"name"				=> "name",
		"email"				=> "email",
		"phone"				=> "phone",
		"first name"		=> "first name",
		"last name"			=> "last name",
		"new password"		=> "new password",
		"again"				=> "again",
		"submit changes"	=> "submit changes",
		"group details"		=> "group details",
		"members"			=> "members",
		"add member"		=> "add member",
		"group not found"	=> "group not found!",
		"add user"			=> "add user",
		"create"			=> "create",
		"add"				=> "add",
		"appointment"		=> "appointment",
		"add group"			=> "add group",
		"appointments"		=> "appointments",
		"appointment details"	=> "appointment details",
		"language"			=> "language",
		"swedish"			=> "swedish",
		"english"			=> "english",
		"attendants"		=> "attendants",
		"latest events"		=> "latest events",
		"events"			=> "events",
		"no events"			=> "no events",
		"date"				=> "date",
		"show"				=> "show",
		"todo"				=> "to do",
		"no such todo item"	=> "no data",
		"no todo items"		=> "nothing to do",
		"no content"		=> "no content",
		"new todo"			=> "new todo",
		"return to list"	=> "return to list",
		"perform"			=> "perform",
		"edit"				=> "edit",
		"reverse list"		=> "reverse list",
		"year"				=> "year",
		"this year"			=> "this year",
		"locale"			=> "locale",
		"timezone"			=> "time zone",
		"friends"			=> "friends",
		"pending requests"	=> "pending requests",
		"add new friend"	=> "add new",
		"email address of user"	=> "email address of user",
		"awaiting authorization" => "awaiting authorization",
		"authorize"			=> "authorize",
		"reject"			=> "reject",
		"none"				=> "none",
		"owner"				=> "owner",
		"no events"			=> "no events!",
		"status"			=> "status",
		"pending"			=> "pending",
		"complete"			=> "complete",
		"toggle status"		=> "toggle status",
		"month_compact"		=> "compact",
		"month_detailed"	=> "detailed",
		"page_comments_etc" => "Comments? Suggestions? Questions? Ask the ",
		"page_webmaster"	=> "webmaster",
		"menu_show"			=> "show menu",
		"menu_hide"			=> "hide menu",
		
		"revents"			=> "revents",
		"revent"			=> "revent",
		"revents_new"		=> "new revent",
		"revents_topic"		=> "topic",
		"revents_rule"		=> "rule",
		"revents_no_such_item"	=> "unknown revent",
		"revents_no_rule"	=> "no rule given",
		"revents_no_items"	=> "no revents",
		
		"index_no_time"		=> "no time",

		"events_flush"		=> "low-light all new events",

		"messages_title"	=> "messages",
		"messages_dst"		=> "recipient",
		"messages_msg"		=> "message",
		"messages_send"		=> "send",
		"messages_no_messages"	=> "no messages",
		"messages_send_new"	=> "send new",
		"messages_remove"	=> "remove",
		"messages_answer"	=> "reply",
		"messages_from"		=> "from",
		"messages_options"	=> "options",
		"printer_friendly"	=> "printer friendly version"
	)
);

function local( $key ) {
	global $g_langid;
	global $g_string;
	
	$result = $g_string[$g_langid][$key];
	
	if ( $result == "" ) {
		return "string '" . $key . "' not translated to language " . $g_langid;
	} else {
		return $result;
	}
}

function create_event( $user, $text, $url, $type ) {
	global $g_sql;
	global $userid;
	
	$query = "
		INSERT INTO
			events
			(user,content,at_time,url,type)
		VALUES
			($user,\"$text\",from_unixtime(unix_timestamp()),\"$url\",\"$type\")
		;";
	
	$g_sql->Query($query);
	
	if ( $userid != $user ) {
		$new_id = $g_sql->InsertID();
	
		$query = "
			INSERT INTO
				new_events
				(user,event,count)
			VALUES
				($user,$new_id,5)
			;";
		
		$g_sql->Query($query);
	}
}

function clear_new_events() {
	global $g_sql;
	global $userid;
	
	// update count
	$query = "
		UPDATE
			new_events
		SET
			count=count-1
		WHERE
			user=$userid;";
	
	$g_sql->Query($query);
	
	// delete any new_events where count=0
	$query = "
		DELETE FROM
			new_events
		WHERE
			(user=$userid) AND
			(count=0)
		;";
	
	$g_sql->Query($query);
}

function flush_new_events() {
	global $g_sql;
	global $userid;
	
	// delete any new_events
	$query = "
		DELETE FROM
			new_events
		WHERE
			(user=$userid)
		;";
	
	$g_sql->Query($query);
}

function ln_ucfirst( $str ) {
	// modified version of ucfirst(), handles "&ouml;" etc.
	if ( $str[0] == '&' && strlen($str) > 1 ) {
		$str[1] = strtoupper($str[1]);
	}
		
	return ucfirst($str);
}

function FatalError( $error ) {
	global $g_layout;
	
	$g_layout->StartPage( "Error" );
	
	print "<h2>Error</h2>\n";
	
	print "$error\n";
	
	$g_layout->EndPage();
	
	exit;
}

function ln_get_locales() {
	$locales = array();
	$locale_data = fopen("locale.alias","r");
	while ( $line = fgets($locale_data) ) {
		if ( ($line[0] != "#") && (strlen($line) > 4) ) {
			$data = preg_split("/\s+/", $line);
			array_push( $locales, $data );
		}
	}
	return $locales;
}


function validate_revent_rule($rule) {
	/*
	* Validate, this string should only contain digits, operators and q variables
	*/
	$sTemp = strtolower($rule);
	$pat = "(\\\$year|\\\$month|\\\$date|\\\$week|\\\$weekday)";
	$replace = "";
	$sTemp = ereg_replace($pat,$replace,$sTemp);
	/*
	* Convert digits and operators to blank
	*/
	$sTemp = strtr($sTemp,"0123456789-+/*()&%|=","                    ");
	/*
	* If the calculation isn't null its invalid
	*/
	return trim($sTemp);
}

function is_valid_revent_rule($rule) {
	return validate_revent_rule($rule) == "";
}

function stripslashes_array($line) {
	foreach ( $line as $key => $value ) {
		$line[$key] = stripslashes($line[$key]);
	}
	return $line;
}

function stripquotes($str) {
	$str = stripslashes($str);
	$str = str_replace("\"","",$str);
	$str = str_replace("'","",$str);
	return $str;
}
	
function get_friends() {
	global $g_sql;
	global $userid;
	
	$query = "
		SELECT 
			users.firstname,
			users.lastname,
			users.id
		FROM 
			users,friends
		WHERE 
			(friends.active = 'true') AND
			(
				(
					(friends.friend1 = users.id) AND
					(friends.friend2 = $userid)
				) OR
				(
					(friends.friend1 = $userid) AND
					(friends.friend2 = users.id)
				)
			)
		ORDER BY
			users.firstname, users.lastname
	;";
	
	return $g_sql->Query($query);
}

function get_groups() {
	global $g_sql;
	global $userid;
	
	$query = "SELECT DISTINCT groups.id,groups.name,groups.owner FROM groups,groupmembers WHERE groupmembers.user=$userid AND groupmembers.grp=groups.id";
	
	return $g_sql->Query($query);
}

function forward_to($url) {
	print "<html><head>\n";
	print "<meta http-equiv=\"refresh\" content=\"0;URL=$url\"></head>";
	print "<body></body>\n";
	print "</html>\n";
	exit;
}
	
function send_notification($message)
{
  $headers = 
    "From: kate@p-sons.com" . "\r\n" .
    "MIME-Version: 1.0" . "\r\n" .
    "Content-type: text/plain; charset=utf-8";

  $ok = mail(
    "kate@p-sons.com", "Kalendarieuppdatering",
    $message, $headers
  );
  
  if ( !$ok )
  {
    print "Problem sending notification! ";
    print "Action aborted.";
    exit;
  }
}

?>
