<?php

require_once("session.php");
require_once("utils.php");
require_once("events.php");
require_once("users.php");
require_once("usertools.php");

if ( isset($action) )
{
  switch ( $action )
  {
    case "login":
      $data = load_user($email);
      if ( $data[_email] != $email )
      {
        print "Ingen anv&auml;ndare med email $email";
        exit;
      }
      if ( $data[_password] != $pass )
      {
        print "Fel l&ouml;senord";
        exit;
      }
      $_SESSION['email'] = $data[_email];
      $_SESSION['loggedIn'] = true;
      if ( $data[_admin] > 0 )
        $_SESSION['admin'] = true;
      break;
   
    case "logout":
      $_SESSION['loggedIn'] = false;
      $_SESSION['admin'] = false;
      break;
      
    case "add":
      if ( !$_SESSION['loggedIn'] || !$_SESSION['admin'] )
      {
        print "Inte inloggad som admin.";
        exit;
      }
      add_event($date, $time, $title, $theme, $speaker, $music, $hidden, $public, $youth, $kids,
        $cafe, $camp, $leader, $other, $organist, $praise, $sound, $ppt);
      break;

    case "edit":
      if ( !$_SESSION['loggedIn'] || !$_SESSION['admin'] )
      {
        print "Inte inloggad som admin.";
        exit;
      }
      $event = load_event($id);
      print "<?xml version='1.0' encoding='UTF-8'?>\n";
      print "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' ";
        print "'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n";
      print "<html><head><link rel='stylesheet' href='style.css' /></head>\n";
      print "<body><div class='event_list'>\n";
      print_editor($event);
      print "<a href='power.php'>Tillbaka</a>\n";
      print "</div></body></html>";
      exit;
      
    case "performEdit":
      if ( !$_SESSION['loggedIn'] || !$_SESSION['admin'] )
      {
        print "Inte inloggad som admin.";
        exit;
      }
      edit_event($id, $date, $time, $title, $theme, $speaker, $music, $hidden, $public, $youth, $kids,
        $cafe, $camp, $leader, $other, $organist, $praise, $sound, $ppt);
      break;
      
    case "delete":
      if ( !$_SESSION['loggedIn'] || !$_SESSION['admin'] )
      {
        print "Inte inloggad som admin.";
        exit;
      }
      delete_event($id);
      break;

    case "nextWeek":
      $_SESSION['week'] += 1;
      if ( $_SESSION['week'] > weeks_in_year($_SESSION['year']) )
      {
        $_SESSION['week'] = 1;
        $_SESSION['year'] += 1;
      }
      header("Location: power.php");
      break;
      
    case "lastWeek":
      $_SESSION['week'] -= 1;
      if ( $_SESSION['week'] == 0 )
      { // switch to last year
        $_SESSION['year'] -= 1;
        $_SESSION['week'] = weeks_in_year($_SESSION['year']);
      }
      header("Location: power.php");
      break;
  }
}

print "<?xml version='1.0' encoding='UTF-8'?>\n";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
<head>
<link rel="stylesheet" href="style.css" />
<title>Kates kalender</title>
</head>

<body>

<?php

/*
 * F�rstasidan - denna plus n�sta vecka, bara publika
 *
 * Kalendarie - en termin �t g�ngen, jan-jun + jul-dec, �ven icke-publika
 *
 * Poweruser-sidan - alla f�lten (plus editl�nkar om inloggad), en vecka �t g�ngen
 *
 * Admin-sidan - l�gga till o ta bort anv�ndare f�r poweruser-sidan
 *
 * */

print "<h1>Massor-av-info-sidan</h1>\n";
if ( $_SESSION['loggedIn'] )
{
  print "<a href='power.php?action=logout'>Logga ut</a><br/>\n";
}

if ( $_SESSION['loggedIn'] )
{
  print "<div class='event_list'>\n";
  print "<h1>L&auml;gg till aktivitet</h1>\n";
  print_editor();
  print "</div>\n";

} else
{
?>
<h2>Logga in</h2>
<form action='power.php' method='post'>
  <input type='hidden' name='action' value='login' />
  Email: <input type='text' name='email' value='' />
  L&ouml;sen: <input type='password' name='pass' value='' />
  <input type='submit' value='Logga in' />
</form>
<?php
}

print_events_for_selected_week(true, $_SESSION['loggedIn'], false, true);

?>
</body>
</html>
