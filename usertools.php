<?php

require_once("session.php");
require_once("utils.php");
require_once("events.php");
require_once("users.php");

function weeks_in_year($year)
{
  $last_week = strftime("%V", strtotime("$year-12-31"));
  if ( $last__week == 1 )
  {
    $last_week = strftime("%V", strtotime("$year-12-26"));
  }
  return $last_week;
}

function events_for_span($from, $to, $onlyPublic=true, $includeHidden=false )
{
  global $g_sql;
  
  if ( $onlyPublic )
    $r = $g_sql->Query("
      SELECT * FROM events 
      WHERE _startTime >= '$from' AND _startTime <= '$to' AND _public > 0 AND _hidden=0
      ORDER BY _startTime ASC"); 
  else
  { 
    if ( $includeHidden )
    {
      $r = $g_sql->Query("
        SELECT * FROM events 
        WHERE _startTime >= '$from' AND _startTime <= '$to' 
        ORDER BY _startTime ASC"); 
    } else
    {
      $r = $g_sql->Query("
        SELECT * FROM events 
        WHERE _startTime >= '$from' AND _startTime <= '$to' AND _hidden=0 
        ORDER BY _startTime ASC"); 
    }
  }

  
  $res = array();

  while ( $line = $g_sql->NextLine($r) )
  {
    array_push($res, $line);
  }

  return $res;
}

function events_for_week($year, $week, $onlyPublic=true, $includeHidden=false)
{
  $start = start_of_week($year, $week);

  $weekStart = strftime("%Y-%m-%d 00:00:00", $start);
  $weekEnd = strftime("%Y-%m-%d 23:59:59", $start+6*24*3600);

  return events_for_span($weekStart, $weekEnd, $onlyPublic, $includeHidden);
}

function events_for_current_week($onlyPublic=true)
{
  global $g_sql;

  $dow = strftime("%u", strtotime("now"));
 
  $weekStart = strftime("%Y-%m-%d 00:00:00", strtotime("now")-($dow-1)*24*3600);
  $weekEnd = strftime("%Y-%m-%d 23:59:59", strtotime($weekStart)+7*24*3600-1);
  
  return events_for_span($weekStart, $weekEnd, $onlyPublic);
}

function print_event($event, $showExtended=false, $showEditLinks=false)
{
?>  <div class='event'>
    <div class='event_time'>
      <?php
      $time = substr($event[_startTime], 11, 5);
      if ( $time != "00:00" )
        print "kl $time";
      ?>
      <div class='event_icons'>
        <?php
        if ($event[_music] != "") 
          print "<img src='gfx/musik.gif' width='16' height='16' 
            alt='Aktivitet med musik' title='Aktivitet med musik'/>";
//        else
//          print "<img src='gfx/blank.gif' width='16' height='16' />";
        if ($event[_youth] > 0)
          print "<img src='gfx/ungdom.gif' width='16' height='16' 
            alt='Ungdomsaktivitet' title='Ungdomsaktivitet'/>";
//        else
//          print "<img src='gfx/blank.gif' width='16' height='16' />";
        if ($event[_kids] > 0)
          print "<img src='gfx/barn.gif' width='16' height='16' 
            alt='Barnaktivitet' title='Barnaktivitet'/>";
//        else
//          print "<img src='gfx/blank.gif' width='16' height='16' />";
        if ($event[_cafe] > 0)
          print "<img src='gfx/cafe.gif' width='16' height='16' 
            alt='Caf&eacute;' title='Caf&eacute;'/>";
        if ($event[_camp] > 0)
          print "<img src='gfx/lager.gif' width='16' height='16' 
            alt='L&auml;ger' title='L&auml;ger'/>";
        if ($event[_hidden] > 0)
          print "<img src='gfx/dold.gif' width='16' height='16' 
            alt='Dold' title='Dold'/>";
          
        ?>
      </div>
    </div> 
    <div class='event_title'><?php print $event[_title]; ?></div>
    <?php
      if ( $event[_theme] != "" )
        print "<div class='event_theme'>$event[_theme]</div>\n";
    ?>
    <div class='event_text'>
      <?php
        if ( $event[_speaker] != "" )
          print "<i>$event[_speaker]</i><br/>\n";
        if ( $event[_music] != "" )
          print "<i>$event[_music]</i><br/>\n";
      ?>
    <?php
      if ( $showExtended ) 
      {
    ?>
        <br/>
        Ledare: <?php print $event[_leader]; ?><br/>
        &Ouml;vrigt: <?php print $event[_other]; ?><br/>
        Organist: <?php print $event[_organist]; ?><br/>
        Lovs&aring;ng: <?php print $event[_praise]; ?><br/>
        Ljud: <?php print $event[_sound]; ?><br/>
        Powerpoint: <?php print $event[_ppt]; ?><br/>
    <?php
      }
      
      if ( $event[_id] > 0 && $showEditLinks )
      {
        print "<br/><div style='float: right;'><form action='power.php' method='post'>";
        print "<input type='hidden' name='action' value='delete' />";
        print "<input type='hidden' name='id' value='$event[_id]' />";
        print "<input type='submit' value='Ta bort' /></form></div>\n";
        
        print "<br/><a href='power.php?action=edit&id=$event[_id]'>Redigera</a>\n";
      }
    ?>
    </div>
    
  </div>
<?php
}

function print_events_for_week($year, $week, $showExtended=false, 
  $showEditLinks=false, $onlyPublic=true, $includeHidden=false)
{
  print_events_for_weeks($year, $week, 1, $showExtended, $showEditLinks, $onlyPublic, true, $includeHidden);
}

function print_events_for_weeks($year, $week, $numWeeks, $showExtended=false, 
  $showEditLinks=false, $onlyPublic=true, $showEmptyWeeks=true, $includeHidden=false)
{
  print "<div class='event_list'>\n";
  
  for ($curr=0; $curr<$numWeeks; $curr++)
  {
    $events = events_for_week($year, $week+$curr, $onlyPublic, $includeHidden);

    if ( count($events) == 0 && !$showEmptyWeeks )
      continue;
?>
<h2>Vecka <?php print strftime("%V", start_of_week($year, $week+$curr)); ?></h2>
<?
  
    if ( count($events) == 0 )
    {
      print "<i>Inget inbokat</i>\n";
      continue;
    }
  
    $lastDate = "";
  
    foreach ($events as $event)
    {
      $date = substr($event[_startTime], 0, 10);
  
      if ( $date != $lastDate )
      { // print date if needed
        print "<div class='event_date'>" . utf8_encode(strftime("%a %d %b", strtotime($date))) . "</div>\n";
        $lastDate = $date;
      }
    
      print_event($event, $showExtended, $showEditLinks);
    }
  }
?>
</div>
<?php
}

function print_events_for_span($from, $to, $showExtended=false, 
  $showEditLinks=false, $onlyPublic=true, $showEmptyWeeks=true)
{
  print "<div class='event_list'>\n";
  
  $target = strftime("%Y %V", $to);
  $curr = $from;
  
  while ( strcmp($target, strftime("%Y %V", $curr)) >= 0 )
  {
    $events = events_for_week(strftime("%Y",$curr), strftime("%V",$curr), $onlyPublic);
    $startofweek = $curr;
    $curr += 7*24*3600; // add one week
    
    if ( count($events) == 0 && !$showEmptyWeeks )
      continue;
?>
<h2>Vecka <?php print strftime("%V", $startofweek); ?></h2>
<?
  
    if ( count($events) == 0 )
    {
      print "<i>Inget inbokat</i>\n";
      continue;
    }
  
    $lastDate = "";
  
    foreach ($events as $event)
    {
      $date = substr($event[_startTime], 0, 10);
  
      if ( $date != $lastDate )
      { // print date if needed
        print "<div class='event_date'>" . utf8_encode(strftime("%a %d %b", strtotime($date))). "</div>\n";
        $lastDate = $date;
      }
    
      print_event($event, $showExtended, $showEditLinks);
    }
  }
?>
</div>
<?php
}

function print_events_for_selected_week($showExtended=false, $showEditLinks=false, 
  $onlyPublic=true, $includeHidden=false)
{
  print "<a href='power.php?action=lastWeek'>F&ouml;reg. vecka</a> ";
  print "<a href='power.php?action=nextWeek'>N&auml;sta vecka</a> ";
  
  print_events_for_week($_SESSION['year'], $_SESSION['week'], $showExtended, 
    $showEditLinks, $onlyPublic, $includeHidden);
}

function print_events_for_current_week($showExtended=false, $showEditLinks=false, $onlyPublic=true)
{
  print_events_for_week(
    strftime("%Y", strtotime("now")), 
    strftime("%V", strtotime("now")), $showExtended, $showEditLinks, $onlyPublic
  );
}

function print_events_for_current_weeks($showExtended=false, $showEditLinks=false, $onlyPublic=true)
{
  print_events_for_weeks(
    strftime("%Y", strtotime("now")), 
    strftime("%V", strtotime("now")), 
    2, $showExtended, $showEditLinks, $onlyPublic
  );
}


function print_editor($event=array())
{
?>

<form action="power.php" method="POST">
<?php
  if ( $event[_id] == "" )
    print "<input type='hidden' name='action' value='add' />\n";
  else {
    print "<input type='hidden' name='action' value='performEdit' />\n";
    print "<input type='hidden' name='id' value='$event[_id]' />\n";
  }
?>

<pre>
Dag    <?php
print "<select name='date'>\n";

$start = start_of_week($_SESSION['year'], $_SESSION['week']);

$oldDate = substr($event[_startTime],0,10);

for ( $d=0; $d<7; $d++ )
{
  $date = strftime("%Y-%m-%d", $start+$d*24*3600);
  if ( $oldDate == $date )
    $selected = "selected";
  else
    $selected = "";
  print "<option value='$date' $selected>". utf8_encode(strftime("%a %d %b", $start+$d*24*3600)) ."</option>\n";
}

print "</select>\n";
?>
Tid    <?php

print "<select name='time'>\n";

$oldTime = substr($event[_startTime], 11, 5);

// 'no time' option
if ( $oldTime == "00:00" )
  $selected = "selected";
else
  $selected = "";
print "<option value='00:00' $selected></option>\n";

$mins = array("00", "15", "30", "45");
for ( $h=6; $h<24; $h++ )
{
  $ht = "$h";
  if ( strlen($ht) == 1 )
    $ht = "0" . $ht;
  foreach ( $mins as $min )
  {
    $selected = "";
    if ( "$ht:$min" == $oldTime )
      $selected = "selected";
    print "<option value='$ht:$min' $selected>$ht:$min</option>\n";
  }
}

print "</select>\n";
?>

Titel  <input type="text" name="title" value="<?php print $event[_title];?>" />

Tema   <input type="text" name="theme" value="<?php print $event[_theme];?>" />

Talare <input type="text" name="speaker" value="<?php print $event[_speaker];?>" />

Musik  <input type="text" name="music" value="<?php print $event[_music];?>" />

Dold   <?php
  $checked = "";
  if ( $event[_hidden] > 0 )
    $checked = "checked=''";
  print "<input type='checkbox' name='hidden' $checked />\n";
?>

Publik <?php
  $checked = "";
  if ( $event[_public] > 0 )
    $checked = "checked=''";
  print "<input type='checkbox' name='public' $checked />\n";
?>

Ungdom <?php
  $checked = "";
  if ( $event[_youth] > 0 )
    $checked = "checked=''";
  print "<input type='checkbox' name='youth' $checked />\n";
?>

Barn   <?php
  $checked = "";
  if ( $event[_kids] > 0 )
    $checked = "checked=''";
  print "<input type='checkbox' name='kids' $checked />\n";
?>

Caf&eacute;   <?php
  $checked = "";
  if ( $event[_cafe] > 0 )
    $checked = "checked=''";
  print "<input type='checkbox' name='cafe' $checked />\n";
?>

L&auml;ger  <?php
  $checked = "";
  if ( $event[_camp] > 0 )
    $checked = "checked=''";
  print "<input type='checkbox' name='camp' $checked />\n";
?>

Ledare       <input type="text" name="leader" value="<?php print $event[_leader];?>" />

&Ouml;vrigt       <input type="text" name="other" value="<?php print $event[_other];?>" />

Organist     <input type="text" name="organist" value="<?php print $event[_organist];?>" />

Lovs&aring;ng      <input type="text" name="praise" value="<?php print $event[_praise];?>" />

Ljud         <input type="text" name="sound" value="<?php print $event[_sound];?>" />

Powerpoint   <input type="text" name="ppt" value="<?php print $event[_ppt];?>" />

<?php

  // submit button
  if ( $event[_id] > 0 )
    print "<input type='submit' value='Spara' />\n";
  else
    print "<input type='submit' value='L&auml;gg till' />\n";
?>
</pre>
</form>

<?
}


function start_of_week($year, $week)
{
  $newyear_time = strtotime("$year-01-01");
  $newyear_dow =strftime("%u", $newyear_time); 
  
  $week_one_time = 0;
  
  if ( $newyear_dow > 4 )
    $week_one_time = $newyear_time + 24*3600 * (8-$newyear_dow);
  else
    $week_one_time = $newyear_time - 24*3600 * ($newyear_dow-1);

  $res = $week_one_time + 24*3600*7 * ($week-1);

  return $res;
}

function end_of_week($year, $week)
{
  return start_of_week($year, $week) + 7*24*3600 - 1;
}

?>
