<?php

require_once("utils.php");

function add_user($email, $pass, $admin)
{
  global $g_sql;
  
  $q = 
    "INSERT INTO
      users
      (_email, _password, _admin)
     VALUES
      (\"$email\", \"$pass\", $admin)";

  $g_sql->Query($q);
}

function edit_user($id, $email, $pass, $admin)
{
  global $g_sql;

  $q = "
    UPDATE
      users
    SET
      _email=\"$email\",
      _password=\"$pass\",
      _admin=$admin
    WHERE
      _id=$id
  ";

  $g_sql->Query($q);
}

function delete_user($id)
{
  global $g_sql;

  $g_sql->Query("DELETE FROM users WHERE _id=$id");
}

function load_user($email)
{
  global $g_sql;

  return $g_sql->GetSingleLine("SELECT * FROM users WHERE _email=\"$email\"");
}

function list_users()
{
  global $g_sql;

  $res = array();

  $r = $g_sql->Query("SELECT * FROM users");

  while ( $line = $g_sql->NextLine($r) )
  {
    array_push($res, $line);
  }

  return $res;
}

function print_users()
{
  print "<h2>Anv&auml;ndare</h2>\n";

  $users = list_users();
  foreach ( $users as $user )
  {
    print "<form action='admin.php' method='post'>\n";
      print "<input type='hidden' name='action' value='edit'/>\n";
      print "<input type='hidden' name='id' value='$user[_id]'/>\n";
      print "Email: <input type='text' name='email' value='$user[_email]' />\n";
      print "L&ouml;sen: <input type='text' name='pass' value='$user[_password]' />\n";
      $checked = "";
      if ( $user[_admin] > 0 )
        $checked = "checked='true'";
      print "Admin <input type='checkbox' name='admin' $checked />\n";
      print "<input type='submit' value='&Auml;ndra' />\n";
    print "</form>\n";
    print "<form action='admin.php' method='post'>\n";
      print "<input type='hidden' name='action' value='delete'/>\n";
      print "<input type='hidden' name='id' value='$user[_id]'/>\n";
      print "<input type='submit' value='Ta bort' />\n";
    print "</form>\n";

    print "<hr/>\n";
  }
}
?>
