<?php

require_once("session.php");
require_once("utils.php");
require_once("users.php");


switch ( $action )
{
  case "login":
    $data = load_user($email);
    if ( $data[_email] != $email )
    {
      print "Ingen anv&auml;ndare med email $email";
      exit;
    }
    if ( $data[_password] != $pass )
    {
      print "Fel l&ouml;senord";
      exit;
    }
    $_SESSION['email'] = $data[_email];
    $_SESSION['loggedIn'] = true;
    if ( $data[_admin] > 0 )        
      $_SESSION['isAdmin'] = true;
    else
      $_SESSION['isAdmin'] = false;
    break;
    
  case "logout":
    $_SESSION['loggedIn'] = false;
    $_SESSION['isAdmin'] = false;
    break;
   
  case "add":
    if ( !$_SESSION['loggedIn'] || !$_SESSION['isAdmin'] )
    {
      print "Inte inloggad som admin.";
      exit;
    }
    if ( $admin == "on" )
      $admin = 1;
    else
      $admin = 0;
    
    add_user($emailadress, $pass, $admin);
    break;

  case "edit":
    if ( !$_SESSION['loggedIn'] || !$_SESSION['isAdmin'] )
    {
      print "Inte inloggad som admin.";
      exit;
    }
    
    if ( $admin == "on" )
      $admin = 1;
    else
      $admin = 0;

    edit_user($id, $email, $pass, $admin);
    break;

  case "delete":
    if ( !$_SESSION['loggedIn'] || !$_SESSION['isAdmin'] )
    {
      print "Inte inloggad som admin.";
      exit;
    }
    delete_user($id);
    break;

}


if ( !$_SESSION['loggedIn'] || !$_SESSION['isAdmin'] )
{
?>
<h1>Logga in</h1>

<form action='admin.php' method='post'>
  <input type='hidden' name='action' value='login' />
  Email: <input type='text' name='email' value='' />
  L&ouml;sen: <input type='password' name='pass' value='' />
  <input type='submit' value='Logga in' />
</form>
<?php
  exit;
}




print "<?xml version='1.0' encoding='UTF-8'?>\n";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<body>

<?php print_users(); ?>
<h2>Ny anv&auml;ndare</h2>
<form action='admin.php' method='post'>
  <input type='hidden' name='action' value='add' />
  Email: <input type='text' name='emailadress' value='' />
  L&ouml;sen: <input type='text' name='pass' value='' />
  Admin: <input type='checkbox' name='admin' />
  <input type='submit' value='Skapa' />
</form>
</body>
</html>

