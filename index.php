<?php

require_once("session.php");
require_once("utils.php");
require_once("events.php");
require_once("users.php");
require_once("usertools.php");


print "<?xml version='1.0' encoding='UTF-8'?>\n";
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>
<head>
<link rel="stylesheet" href="style.css" />
<title>Kates kalender</title>
</head>

<body>

<?php

print "<h1>F&ouml;rstasidan</h1>\n";
print_events_for_current_weeks();

print "<h1>Kalendarie</h1>\n";
print_events_for_span(
  start_of_week(strftime("%Y"), strftime("%V")),
  strtotime("now")+180*24*3600,
  false, false, false, false);
?>
</body>
</html>
