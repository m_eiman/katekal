<?php

session_start();

if ( !isset($_SESSION['year']) )
{
  $_SESSION['year'] = strftime("%Y", strtotime("now"));
  $_SESSION['week'] = strftime("%V", strtotime("now"));
  $_SESSION['loggedIn'] = false;
  $_SESSION['isAdmin'] = false;
}

?>
