<?php

require_once("utils.php");


function load_event($id)
{
  global $g_sql;

  return $g_sql->GetSingleLine("SELECT * FROM events WHERE _id=$id");
}

function add_event($date, $time, $title, $theme, $speaker, $music, $hidden, $public, $youth, $kids,
  $cafe, $camp, $leader, $other, $organist, $praise, $sound, $ppt
)
{
  global $g_sql;
  
  if ( $hidden == "on" )
    $hidden = 1;
  else
    $hidden = 0;
  
  if ( $public == "on" )
    $public = 1;
  else
    $public = 0;
  
  if ( $youth == "on" )
    $youth = 1;
  else
    $youth = 0;
  
  if ( $kids == "on" )
    $kids = 1;
  else
    $kids = 0;

  if ( $cafe == "on" )
    $cafe = 1;
  else
    $cafe = 0;

  if ( $camp == "on" )
    $camp = 1;
  else
    $camp = 0;

  $msg = "Ny aktivitet av $_SESSION[email]:
  Datum:    $date $time
  Titel:    $title
  Tema:     $theme
  Talare:   $speaker
  Ovrigt:   $other
  Organist: $organist
  Lovsang:  $praise
  Ljud:     $sound
  PPT:      $ppt
  Dold:     $hidden
  Publik:   $public
  Ungdom:   $youth
  Barn:     $kids
  Cafe:     $cafe
  Lager:    $camp
  ";
  send_notification($msg);

  $q = "
    INSERT INTO
      events
      (_startTime, _title, _theme, _speaker, _music, _hidden, _public, _youth, _kids,
      _cafe, _camp, _leader, _other, _organist, _praise, _sound, _ppt)
    VALUES
      (\"$date $time\", \"$title\", \"$theme\", \"$speaker\", \"$music\",
      $hidden, $public, $youth, $kids, $cafe, $camp, \"$leader\", \"$other\", \"$organist\", 
      \"$praise\", \"$sound\", \"$ppt\"
      )
  ";
  
  $g_sql->Query($q);
}

function edit_event($id, $date, $time, $title, $theme, $speaker, $music, $hidden,
$public, $youth, $kids, $cafe, $camp, $leader, $other, $organist, $praise, $sound, $ppt)
{
  global $g_sql;

  if ( $hidden == "on" )
    $hidden = 1;
  else
    $hidden = 0;
  
  if ( $public == "on" )
    $public = 1;
  else
    $public = 0;
  
  if ( $youth == "on" )
    $youth = 1;
  else
    $youth = 0;
  
  if ( $kids == "on" )
    $kids = 1;
  else
    $kids = 0;
  
  if ( $cafe == "on" )
    $cafe = 1;
  else
    $cafe = 0;
  
  if ( $camp == "on" )
    $camp = 1;
  else
    $camp = 0;
  
  $e = load_event($id);
  $msg = "Aktivitet uppdaterad av $_SESSION[email]:

Gamla data:
  Datum:    $e[_startTime]
  Titel:    $e[_title]
  Tema:     $e[_theme]
  Talare:   $e[_speaker]
  Ovrigt:   $e[_other]
  Organist: $e[_organist]
  Lovsang:  $e[_praise]
  Ljud:     $e[_sound]
  PPT:      $e[_ppt]
  Dold:     $e[_hidden]
  Publik:   $e[_public]
  Ungdom:   $e[_youth]
  Barn:     $e[_kids]
  Cafe:     $e[_cafe]
  Lager:    $e[_camp]

Nya data:
  Datum:    $date $time
  Titel:    $title
  Tema:     $theme
  Talare:   $speaker
  Ovrigt:   $other
  Organist: $organist
  Lovsang:  $praise
  Ljud:     $sound
  PPT:      $ppt
  Dold:     $hidden
  Publik:   $public
  Ungdom:   $youth
  Barn:     $kids
  Cafe:     $cafe
  Lager:    $camp
  ";
  send_notification($msg);
  
  $q = "
    UPDATE
      events
    SET
      _startTime=\"$date $time\", 
      _title=\"$title\", 
      _theme=\"$theme\", 
      _speaker=\"$speaker\", 
      _music=\"$music\",
      _hidden=$hidden,
      _public=$public,
      _youth=$youth,
      _kids=$kids,
      _cafe=$cafe,
      _camp=$camp,
      _leader=\"$leader\",
      _other=\"$other\",
      _organist=\"$organist\",
      _praise=\"$praise\",
      _sound=\"$sound\",
      _ppt=\"$ppt\"
    WHERE
      _id=$id
  ";
  
  $g_sql->Query($q);
}

function delete_event($id)
{
  global $g_sql;
  
  $e = load_event($id);
  
  $msg = "Aktivitet borttagen av $_SESSION[email]:

  Datum:    $e[_startTime]
  Titel:    $e[_title]
  Tema:     $e[_theme]
  Talare:   $e[_speaker]
  Ovrigt:   $e[_other]
  Organist: $e[_organist]
  Lovsang:  $e[_praise]
  Ljud:     $e[_sound]
  PPT:      $e[_ppt]
  Publik:   $e[_public]
  Ungdom:   $e[_youth]
  Barn:     $e[_kids]
  Cafe:     $e[_cafe]
  Lager:    $e[_camp]
  ";
  send_notification($msg);
  
  $g_sql->Query("DELETE FROM events WHERE _id=$id");
}

?>
